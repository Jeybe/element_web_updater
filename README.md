# Element Web updater

This is a little Python script I wrote to update an Element Web installation.

## What does this script do

It determines the latest v1 release of Element Web, downloads it and moves it to the specified path.

## Configuration

At the top of the script there is a commented configuration section. The variables within that section are meant to be edited to fit your need. Below is a short explanation of what the specific configuration variables are good for.

### `element_web_install_path`

This is the path to the directory where your Element Web installation is located. By default this is set to `/var/www/element_web`.

## Installing Element Web for the first time with this script

Altough the main purpose of this script is to update an existing Element Web instance, it can also be used to streamline the process of installing Element Web. To do so, just run adjust the configure options to your likes, run it and follow the [installation docs from Element Web](https://github.com/vector-im/element-web) to configure it to your needs.

## Licence

All files within this repository are licenced under the [MIT Licence](LICENCE).
