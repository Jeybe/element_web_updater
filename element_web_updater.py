import requests
from bs4 import BeautifulSoup
import re
import tarfile
import shutil
import os

# ---- Configuration ----

element_web_install_path="/var/www/element_web"

# ---- End of configuration ----

element_web_code_hoster = "https://github.com"
element_web_release_page = "https://github.com/vector-im/element-web/releases/latest"

def extract_links(url):
        reqs = requests.get(url)
        soup = BeautifulSoup(reqs.text, 'html.parser')
        
        urls = []
        for link in soup.find_all('a'):
            urls.append(link.get('href'))
        return urls

def find_element_web_release_path():
    release_page_links = extract_links(element_web_release_page)
    for link in release_page_links:
        if link.find("element-v1") != -1:
            return link
    return None

def find_element_web_release_url():
    conversejs_release_url = element_web_code_hoster + find_element_web_release_path()
    return conversejs_release_url

def install_element_web_release():
    # download tarball
    url = find_element_web_release_url()
    req = requests.get(url)
    filename = url.split('/')[-1]
    
    with open(filename, 'wb') as output_file:
        output_file.write(req.content)
    # extract tarball
    tarball = tarfile.open(filename)
    tarball.extractall()
    tarball.close()
    # move files
    dirname = filename.rsplit(".",2)
    os.remove(filename)
    shutil.move(dirname[0],element_web_install_path)
    
    

if __name__ == "__main__":
    install_element_web_release()
